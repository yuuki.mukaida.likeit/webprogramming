package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッション
		HttpSession session = request.getSession();
				
		// ログイン時に保存したセッション内のユーザ情報を取得
		User userInfo = (User) session.getAttribute("userInfo");
		
		if (userInfo != null) {
			// ユーザ一覧のサーブレットにリダイレクト
		    response.sendRedirect("UserListServlet");
		    return;
		
		} else {
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータの文字コードを指定
	    request.setCharacterEncoding("UTF-8");
	    
	    // リクエストパラメータの入力項目を取得
	    String loginId = request.getParameter("loginid");
	    String password = request.getParameter("password");
	    
	    // パスワードの暗号化
	    PasswordEncorder passwordEncorder = new PasswordEncorder();
	    String encodestr = passwordEncorder.encordPassword(password);
	    
	    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
	    UserDao userDao = new UserDao();
	    User user = userDao.findByLoginInfo(loginId, encodestr);
	    
	    /** テーブルに該当のデータが見つからなかった場合 **/
	    if (user == null) {
	        // リクエストスコープにエラーメッセージをセット
	        request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
	        // 入力したログインIDを画面に表示するために値をセット
	        request.setAttribute("loginId", loginId);

	        // ログインjspにフォワード
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
	        dispatcher.forward(request, response);
	        return;
	    }
	    
	    /** テーブルに該当のデータが見つかった場合 **/
	    // セッションにユーザの情報をセット
	    HttpSession session = request.getSession();
	    session.setAttribute("userInfo", user);

	    // ユーザ一覧のサーブレットにリダイレクト
	    response.sendRedirect("UserListServlet");
	}

}
