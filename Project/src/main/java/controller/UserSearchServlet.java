package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSearchServlet
 */
@WebServlet("/UserSearchServlet")
public class UserSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッション
		HttpSession session = request.getSession();
		
		// ログイン時に保存したセッション内のユーザ情報を取得
		User userInfo = (User) session.getAttribute("userInfo");
		
		if (userInfo == null) {
			// ログインのサーブレットにリダイレクト
		    response.sendRedirect("UserLoginServlet");
		    return;
		
		} else {
			// ユーザ一覧のサーブレットにリダイレクト
		    response.sendRedirect("UserListServlet");
		    return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータの文字コードを指定
	    request.setCharacterEncoding("UTF-8");
	    
	    // リクエストパラメータの入力項目を取得
	    String loginId = request.getParameter("user-loginid");
	    String name = request.getParameter("user-name");
	    String birthDateStart = request.getParameter("date-start");
	    String birthDateEnd = request.getParameter("date-end");
	    
	    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
	    UserDao userDao = new UserDao();
	    List<User> userList = userDao.userSearch(loginId, name, birthDateStart, birthDateEnd);
	    
	    // リクエストスコープにユーザ一覧情報をセット
	    request.setAttribute("userList", userList);
	    
	    // ユーザ一覧のjspにフォワード
	    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
	    dispatcher.forward(request, response);
	}

}
