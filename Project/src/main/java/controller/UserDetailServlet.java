package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッション
		HttpSession session = request.getSession();
						
		// ログイン時に保存したセッション内のユーザ情報を取得
		User userInfo = (User) session.getAttribute("userInfo");
						
		if (userInfo == null) {
			// ログインのサーブレットにリダイレクト
		    response.sendRedirect("UserLoginServlet");
			return;
		
		} else {
			// リクエストパラメータの文字コードを指定
		    request.setCharacterEncoding("UTF-8");
			
			// リクエストパラメータの入力項目を取得
		    String str = request.getParameter("id");
		    int id = Integer.parseInt(str);
		    
		    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		    UserDao userDao = new UserDao();
		    User user = userDao.userDetail(id);
		    
		    // メソッドの戻り値をリクエストスコープにセット
		    request.setAttribute("user", user);
		    
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
