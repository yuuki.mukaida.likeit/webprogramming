package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッション
		HttpSession session = request.getSession();
				
		// ログイン時に保存したセッション内のユーザ情報を取得
		User userInfo = (User) session.getAttribute("userInfo");
		
		if (userInfo == null) {
			// ログインのサーブレットにリダイレクト
		    response.sendRedirect("UserLoginServlet");
		    return;
		
		} else {
			// リクエストパラメータの文字コードを指定
		    request.setCharacterEncoding("UTF-8");
		    
		    // リクエストパラメータの入力項目を取得
		    String str = request.getParameter("id");
		    int id = Integer.parseInt(str);
		    
		    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		    UserDao userDao = new UserDao();
		    User user = userDao.findById(id);
		    
		    // メソッドの戻り値をリクエストスコープにセット
		    request.setAttribute("user", user);
		    
		    //フォワード
		  	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		  	dispatcher.forward(request, response);
		  	return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        
        // リクエストパラメータの入力項目を取得
        String loginId = request.getParameter("user-id");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String date = request.getParameter("birth-date");
        
	    /** 更新に失敗した場合 **/
	    if (!password.equals(passwordConfirm)) {
	    	// String型からDate型へ変換
	        Date birthDate = Date.valueOf(date);
	    	
	    	// リクエストスコープにエラーメッセージをセット
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
	    	
	    	// 入力した内容を画面に表示するために値をセット
		    User user = new User(loginId, name, birthDate);
	    	request.setAttribute("user", user);
	    	
	    	// ユーザー更新のjspにフォワード
		    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		    dispatcher.forward(request, response);
		    return;
	    
	    } else if (name == "") {
	    	// String型からDate型へ変換
	        Date birthDate = Date.valueOf(date);
	    	
	    	// リクエストスコープにエラーメッセージをセット
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
	    	
	    	// 入力した内容を画面に表示するために値をセット
	    	User user = new User(loginId, name, birthDate);
	    	request.setAttribute("user", user);
	    	
	    	// ユーザー更新のjspにフォワード
		    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		    dispatcher.forward(request, response);
		    return;
	    
	    } else if (date == "") {
	    	// リクエストスコープにエラーメッセージをセット
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
	    	
	    	// 入力した内容を画面に表示するために値をセット
	    	User user = new User(loginId, name);
	    	request.setAttribute("user", user);
	    	
	    	// ユーザー更新のjspにフォワード
		    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		    dispatcher.forward(request, response);
		    return;
	    }
	    
	    /** 更新に成功した場合 **/
	    // パスワードの暗号化
	    PasswordEncorder passwordEncorder = new PasswordEncorder();
	    String encodestr = passwordEncorder.encordPassword(password);
	    
	    // String型からDate型へ変換
        Date birthDate = Date.valueOf(date);
        
        if (password == "" && passwordConfirm == "") {
        	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    		// パスワード以外のデータを更新
        	UserDao userDao = new UserDao();
        	userDao.userUpdate(loginId, name, birthDate);
        
        } else {
        	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    		// データを更新
    	    UserDao userDao = new UserDao();
    	    userDao.userUpdate(loginId, encodestr, name, birthDate);
        }
	    	
	    // ユーザ一覧のサーブレットにリダイレクト
	    response.sendRedirect("UserListServlet");
	}

}
