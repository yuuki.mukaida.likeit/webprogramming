package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {
	
	public String encordPassword(String password) {

		//変換したい文字列を入力
		String str = password;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュ関数の種類(今回はMD5)
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(str.getBytes(charset));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String encodestr = DatatypeConverter.printHexBinary(bytes);

		//暗号化結果の出力
		System.out.println(encodestr);
		
		return encodestr;
	}

}
