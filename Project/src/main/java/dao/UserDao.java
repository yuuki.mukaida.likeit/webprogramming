package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {
	
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
	    try {
	      // データベースへ接続
	      conn = DBManager.getConnection();

	      // SELECT文を準備
	      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
	      
	      // SELECTを実行し、結果表を取得
	      PreparedStatement pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1, loginId);
	      pStmt.setString(2, password);
	      ResultSet rs = pStmt.executeQuery();
	      
	      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	      if (!rs.next()) {
	        return null;
	      }
	      
	      // 必要なデータのみインスタンスのフィールドに追加
	      String loginIdData = rs.getString("login_id");
	      String nameData = rs.getString("name");
	      return new User(loginIdData, nameData);
	      
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	        }
	      }
	    }
	}
	
	public List<User> findAll() {
	    Connection conn = null;
	    List<User> userList = new ArrayList<User>();

	    try {
	      // データベースへ接続
	      conn = DBManager.getConnection();

	      // SELECT文を準備
	      // 管理者以外を取得
	      String sql = "SELECT * FROM user WHERE login_id != 'admin'";

	      // SELECTを実行し、結果表を取得
	      Statement stmt = conn.createStatement();
	      ResultSet rs = stmt.executeQuery(sql);

	      // 結果表に格納されたレコードの内容を
	      // Userインスタンスに設定し、ArrayListインスタンスに追加
	      while (rs.next()) {
	        int id = rs.getInt("id");
	        String loginId = rs.getString("login_id");
	        String name = rs.getString("name");
	        Date birthDate = rs.getDate("birth_date");
	        String password = rs.getString("password");
	        boolean isAdmin = rs.getBoolean("is_admin");
	        Timestamp createDate = rs.getTimestamp("create_date");
	        Timestamp updateDate = rs.getTimestamp("update_date");
	        User user =
	            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

	        userList.add(user);
	      }
	    } catch (SQLException e) {
	      e.printStackTrace();
	      return null;
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	        }
	      }
	    }
	    return userList;
	}
	
	public void userAdd(String loginId, String password, String name, Date birthDate) {
		Connection conn = null;
		try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  // INSERT文を準備
		  String sql = "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES (?, ?, ?, ?, NOW(), NOW())";
		  
		  // INSERT文を実行
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setString(1, loginId);
		  pStmt.setString(2, name);
		  pStmt.setDate(3, birthDate);
		  pStmt.setString(4, password);
		  pStmt.executeUpdate();
		  
	    } catch (SQLException e) {
	        e.printStackTrace();   
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    }
	}
	
	public User userDetail(int id) {
		Connection conn = null;
		
	    try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  // SELECT文を準備
		  String sql = "SELECT * FROM user WHERE id = ?";
		  
		  // SELECTを実行し、結果表を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setInt(1, id);
		  ResultSet rs = pStmt.executeQuery();
		  
		  // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	      if (!rs.next()) {
	        return null;
	      }
	      
	      // 必要なデータのみインスタンスのフィールドに追加
	      String loginId = rs.getString("login_id");
	      String name = rs.getString("name");
	      Date birthDate = rs.getDate("birth_date");
	      Timestamp createDate = rs.getTimestamp("create_date");
	      Timestamp updateDate = rs.getTimestamp("update_date");
	      return new User(loginId, name, birthDate, createDate, updateDate);
		  
	    } catch (SQLException e) {
		  e.printStackTrace();
		  return null;
		} finally {
		  // データベース切断
		  if (conn != null) {
		    try {
		      conn.close();
		    } catch (SQLException e) {
		      e.printStackTrace();
		      return null;
		    }
		  }
		}
	}
	
	public User findById(int id) {
		Connection conn = null;
		try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  // SELECT文を準備
		  String sql = "SELECT * FROM user WHERE id = ?";
		  
		  // SELECTを実行し、結果表を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setInt(1, id);
		  ResultSet rs = pStmt.executeQuery();
		  
		  // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	      if (!rs.next()) {
	        return null;
	      }
	      
	      // 必要なデータのみインスタンスのフィールドに追加
	      String loginId = rs.getString("login_id");
	      String name = rs.getString("name");
	      Date birthDate = rs.getDate("birth_date");
	      return new User(loginId, name, birthDate);
	      
		} catch (SQLException e) {
		  e.printStackTrace();
		  return null;
	    } finally {
	      // データベース切断
		  if (conn != null) {
			try {
			  conn.close();
			} catch (SQLException e) {
			  e.printStackTrace();
			  return null;
			}
	      }
	    }
	}
	
	public void userUpdate(String loginId, String password, String name, Date birthDate) {
		Connection conn = null;
		try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		   
		  // UPDATE文を準備
		  String sql = "UPDATE user SET name = ?, birth_date = ?, password = ? WHERE login_id = ?";
		   
		  // UPDATE文を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setString(1, name);
		  pStmt.setDate(2, birthDate);
		  pStmt.setString(3, password);
		  pStmt.setString(4, loginId);
		  pStmt.executeUpdate();
		   
		} catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    } 
	}
	
	public void userDelete(String loginId) {
		Connection conn = null;
		try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  //DELETE文を準備
		  String sql = "DELETE FROM user WHERE login_id = ?";
		  
		  //DELETE文を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setString(1, loginId);
		  pStmt.executeUpdate();
		  
		} catch (SQLException e) {
	        e.printStackTrace();   
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    }
	}
	
	public User findByLoginId(String loginId) {
		Connection conn = null;
	    try {
	      // データベースへ接続
	      conn = DBManager.getConnection();
	      
	      // SELECT文を準備
	      String sql = "SELECT * FROM user WHERE login_id = ?";
	      
	      // SELECTを実行し、結果表を取得
	      PreparedStatement pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1, loginId);;
	      ResultSet rs = pStmt.executeQuery();
	      
	      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	      if (!rs.next()) {
	        return null;
	      }
	      
	      // 必要なデータのみインスタンスのフィールドに追加
	      String loginIdData = rs.getString("login_id");
	      return new User(loginIdData);
	      
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	        }
	      }
	    }
	}
	
	public void userUpdate(String loginId, String name, Date birthDate) {
		Connection conn = null;
	    try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  //UPDATE文を準備
		  String sql = "UPDATE user SET name = ?, birth_date = ? WHERE login_id = ?";
		  
		  // UPDATE文を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql);
		  pStmt.setString(1, name);
		  pStmt.setDate(2, birthDate);
		  pStmt.setString(3, loginId);
		  pStmt.executeUpdate();
		  
	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    }
	}
	
	public List<User> userSearch(String loginId, String name, String birthDateStart, String birthDateEnd) {
		Connection conn = null;
		List<String> list = new ArrayList<String>();
		List<User> userList = new ArrayList<User>();
		
	    try {
		  // データベースへ接続
		  conn = DBManager.getConnection();
		  
		  // SELECT文を準備
		  String str = "SELECT * FROM user WHERE login_id != 'admin'";
		  
		  // strに代入した文字を連結させるためにStringBuilderのインスタンスを宣言
		  StringBuilder sql = new StringBuilder(str);
		  
          if (!loginId.equals("")) {
        	  sql.append("and login_id = ?");
        	  list.add(loginId);
          }
          
          if (!name.equals("")) {
        	  sql.append("and name like ?");
        	  list.add("%" + name + "%");
          }
          
          if (!birthDateStart.equals("")) {
        	  sql.append("and birth_date >= ?");
        	  list.add(birthDateStart);
          }
          
          if (!birthDateEnd.equals("")) {
        	  sql.append("and birth_date <= ?");
        	  list.add(birthDateEnd);
          }
		  
          // SELECTを実行し、結果表を取得
		  PreparedStatement pStmt = conn.prepareStatement(sql.toString());
		  for(int i = 0; i < list.size(); i++) {
			  pStmt.setString(i + 1, list.get(i));
		  }
		  ResultSet rs = pStmt.executeQuery();
          
		  // 結果表に格納されたレコードの内容を
	      // Userインスタンスに設定し、ArrayListインスタンスに追加
	      while (rs.next()) {
	        int id = rs.getInt("id");
	        String userLoginId = rs.getString("login_id");
	        String userName = rs.getString("name");
	        Date birthDate = rs.getDate("birth_date");
	        String password = rs.getString("password");
	        boolean isAdmin = rs.getBoolean("is_admin");
	        Timestamp createDate = rs.getTimestamp("create_date");
	        Timestamp updateDate = rs.getTimestamp("update_date");
	        User user =
	            new User(id, userLoginId, userName, birthDate, password, isAdmin, createDate, updateDate);

	        userList.add(user);
	      }
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	        }
	      }
	    }
	    return userList;
	}
}
